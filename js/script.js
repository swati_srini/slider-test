// Your code here.
(function( $ ) {
	$.fn.carousel = function( options ) {
		var settings = $.extend({
            // These are the defaults.
            auto: 'false',
            timer: '3000',
            vertical: 'false'
        }, options );

		var carousel = this;
		var carouselItems = this.find('li');
		this.find('ul').addClass('unorderedlist');
		carouselItems.addClass('carousel-item');
		carouselItems.first().addClass('current');

		if(settings.auto == 'false'){
			this.siblings().click( function() {
				var currentItem = carousel.find('li.current');
				if($(this).hasClass("carousel-control-prev")) {
					if(typeof(currentItem.prev()) != 'undefined' && currentItem.prev().length > 0) {
						currentItem.removeClass('current');
						currentItem.prev().addClass('current');
					}
				} else {
					if(typeof(currentItem.next()) != 'undefined' && currentItem.next().length > 0) {
						currentItem.removeClass('current');
						currentItem.next().addClass('current');
					}
				}
			});
		}
		else {
			
		}
	};
})( jQuery );